<?php
class Users_model extends CI_Model {

		/**
		 * Consturct function
		 */
        public function __construct()
        {
                $this->load->database();
        }

        /**
         * Get Users function: used to get all user from database
         * @param 	integer $offset (optional)
         * @param 	integer $limit (optional)
         * @return 	Array 	a list with all user into database. 
         */
        public function get_users($offset = 0, $limit = 1)
		{		
				$result = array();
				$filter = $this->input->get('filter');

		        if ($filter)
		        {
			        $this->db->like('name', $filter);
			        $this->db->or_like('email', $filter);
		        }

		        $result['qty'] = $this->db->count_all_results('users', false);
                $this->db->limit($limit, $offset);
                $this->db->order_by('id', 'DESC');
                $query = $this->db->get();
		        $result['list'] = $query->result_array(); 
                return $result;
		        
		}

        /**
         * Get User function: used to get an user from database
         * @param 	integer 	$id 
         * @return 	Objetc 		an user from database matching with supplied id. 
         */
		public function get_user($id)
		{
		        $query = $this->db->get_where('users', array('id' => $id));
		        return $query->row();
		}

		/**
         * Delete User function: used to delete an user from database
         * @param 	integer 	$id 
         * @return 	 
         */
		public function delete_user($id)
		{
		        $query = $this->db->delete('users', array('id' => $id));
				$query = $this->db->delete('user_roles', array('user_id' => $id));
		        return $query;
		}

        /**
         * Get User by Email function: used to get an user from database by email address
         * @return 	Object 	an user from database matching with supplied email. 
         */
		public function check_email()
		{
		        $this->db->where('email',$this->input->get('email'));
			    $query = $this->db->get('users');

			    if ($query->num_rows() > 0){
			    	$user = $query->row();
			    	if ($user->id == $this->input->get('id')) {
			    		return false;
			    	}
			        return true;
			    }
			    else{
			        return false;
			    }
		}

		/**
         * Set User function: used to create/update an user to database
         * @param 
         * @return  
         */
		public function set_user()
		{

		    $data = array(
		        'name' => $this->input->post('name'),
		        'email' => $this->input->post('email'),
		        'phone' => $this->input->post('phone'),
		        'age' => $this->input->post('age')
		    );

		    if (($this->input->post('id'))) { //Editing user data
				$user_id = $this->input->post('id');
		    	$this->db->where('id', $user_id);
				$this->db->update('users', $data);
				$this->db->delete('user_roles', array('user_id' => $user_id));
		    }else{ // Creating new user
			    $this->db->insert('users', $data);
				$user_id = $this->db->insert_id();
		    }

			$roles = $this->input->post('role');
			$user_roles = array();

			foreach ($roles as $value) {
				array_push($user_roles, array('user_id' => $user_id,'role_id' => $value));
			}

		    return $this->db->insert_batch('user_roles', $user_roles);
		}

		/**
         * Get Roles function: used to get user's roles
         * @param 	integer 	$user_id
         * @return 	array 		a list with all user's roles assocated.
         */
		public function get_roles($user_id)
		{
			$query = $this->db->get_where('user_roles', array('user_id' => $user_id));
	        return $query->result_array();
		}
}

