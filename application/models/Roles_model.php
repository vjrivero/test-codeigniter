<?php
class Roles_model extends CI_Model {

		/**
		 * Consturct function
		 */
        public function __construct()
        {
                $this->load->database();
        }

        /**
         * Get Roles function: used to get all roles from database
         * @return 	Array 	a list with all roles into database. 
         */
        public function get_roles()
		{
                $query = $this->db->get('roles');
                return $query->result_array();
		}

		/**
         * Get Roles to Select function: used to get a role list formated to fill input select 
         * @return 	Array 	a list with all roles into database. 
         */
		public function get_roles_to_select()
		{
				$query = $this->db->get('roles');
				$options = array();
				$result = $query->result_array();
				foreach ($result as $item) {
					$options[$item['id']] = $item['name'];
				}
    	        return $options;	
		}

}