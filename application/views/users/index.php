
<?php if ($this->session->flashdata('success_message')) : ?>
<div class="alert alert-success alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<?php echo $this->session->flashdata('success_message'); ?>
</div>
<?php endif; ?>

<div class="pull-right">
	<?php echo anchor('users/add', 'Add User', array('title' => 'Add new user', 'class' => 'btn btn-primary')); ?>
</div>

<!-- Form Search -->
<div class="pull-left">
<?php 
	$searchInput = "";
	if (isset($param)) {
		$searchInput = $param;
	}

	echo form_open('users/', array('class' => 'form-inline form-search','id' => 'form-search'));  ?>

    <div class="form-group">
	    <div class="input-group">
	      <?php echo form_input(array('name' => 'searchInput','id' => 'searchInput','type' => 'text','placeholder' => 'Search','class' => 'form-control'), $searchInput); ?>
	      <div class="input-group-btn">
			<button type="submit" class="btn btn-default" ><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
	      </div>
	    </div>
	</div>

<?php echo form_close(); ?>
</div>
<!-- End Form Search -->

<table class="table table-striped table-hover">
	<thead>
		<tr>
			<th class="col-md-1">#</th>
			<th class="col-md-4">User Name</th>
			<th class="col-md-5">Email</th>
			<th class="col-md-2">Actions</th>
		</tr>
	</thead>
	<tbody>

	<?php if (empty($users)) : ?>
		<tr>
			<td colspan="4">No record to show</td>
		</tr>
	<?php else : ?>
		<?php foreach ($users as $user): ?>
			<tr>
				<td><?php echo $user['id']; ?></td>
				<td><?php echo $user['name']; ?></td>
				<td><?php echo $user['email']; ?></td>
				<td>
					<?php echo anchor('users/'.$user['id'], '<span class="glyphicon glyphicon-search" aria-hidden="true"></span>', array('title' => 'View user details')); ?>&nbsp;
					<?php echo anchor('users/edit/'.$user['id'], '<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>', array('title' => 'Edit user details')); ?>&nbsp;
					<?php echo anchor('users/delete/'.$user['id'], '<span class="glyphicon glyphicon-trash text-danger" aria-hidden="true"></span>', array('title' => 'Delete user','class' => 'delete-user','data-user' => $user['name'], 'onClick' => "return false;")); ?>
				</td>
			</tr>
		<?php endforeach; ?>
	<?php endif; ?>

	</tbody>
</table>

<!-- Pagination -->
<div class="text-center">
	<nav aria-label="Page navigation">
		<?php echo $this->pagination->create_links(); ?>
	</nav>
</div>

<!-- Modal -->
<div class="modal fade bs-example-modal-sm" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Confirm</h4>
      </div>
      <div class="modal-body">
        Are you sure to delete user <span class="user-name"></span>?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary delete-button" data-loading-text="Deleting..." onClick="deleteUser()">Delete</button>
      </div>
    </div>
  </div>
</div>


<script type = 'text/javascript' src = "<?php echo base_url(); ?>js/users.js"></script> 