
<?php echo validation_errors(); ?>

<?php 
	$hidden = array();
	$name = "";
	$email = "";
	$phone = "";
	$age = "";

	if (isset($user)) {
		$hidden['id'] = $user->id;
		$name = $user->name;
		$email = $user->email;
		$phone = $user->phone;
		$age = $user->age;
	}

	echo form_open('users/add', array('class' => 'form-horizontal user-form','id' => 'user-form'), $hidden);  ?>

	<div class="form-group has-feedback input-name">
		<?php echo form_label('User Name', 'name', array('class' => 'col-sm-2 control-label')); ?>
		<div class="col-sm-10">
			<?php echo form_input(array('name' => 'name','id' => 'name','type' => 'text','placeholder' => 'Name','class' => 'form-control'), $name); ?>
			<span class="glyphicon glyphicon-remove form-control-feedback hidden" aria-hidden="true"></span>
  			<span id="inputErrorName" class="help-block hidden">Name can't be empty.</span>
		</div>
    </div>

    <div class="form-group has-feedback input-email">
	    <?php echo form_label('Email', 'email', array('class' => 'col-sm-2 control-label')); ?>
	    <div class="col-sm-10">
	    	<?php echo form_input(array('name' => 'email','id' => 'email','type' => 'email','placeholder' => 'example@email.com','class' => 'form-control'), $email); ?>
			<span class="glyphicon glyphicon-remove form-control-feedback hidden" aria-hidden="true"></span>
  			<span id="inputErrorEmail" class="help-block email-invalid hidden">Enter a valid email.</span>
  			<span id="inputErrorEmail" class="help-block email-exist hidden">Email already exist, please enter a new one.</span>
  		</div>
    </div>

    <div class="form-group">
	    <?php echo form_label('Phone Number', 'phone', array('class' => 'col-sm-2 control-label')); ?>
	    <div class="col-sm-10">
	    	<?php echo form_input(array('name' => 'phone','id' => 'phone','placeholder' => 'XXX-XXXXXX','type' => 'number','class' => 'form-control'), $phone);	 ?>
  		</div>
    </div>

    <div class="form-group">
	    <?php echo form_label('Age', 'age', array('class' => 'col-sm-2 control-label')); ?>
	    <div class="col-sm-10">
	    	<?php echo form_input(array('name' => 'age','id' => 'age','type' => 'number','placeholder' => '0','min' => '0','class' => 'form-control'), $age); ?>
  		</div>
    </div>

    <div class="form-group input-roles">
	    <?php echo form_label('Role', 'role', array('class' => 'col-sm-2 control-label')); ?>
	    <div class="col-sm-10">
	    	<?php echo form_multiselect('role[]', $roles, $selected,array('class' => 'form-control')); ?>
  			<span id="inputErrorName" class="help-block hidden">Select at least one role.</span>
  		</div>
	</div>
    
    <?php //echo form_submit('submit', $title, array('class' => 'btn btn-primary submit-form-user'));  ?>
    
    <?php echo anchor('#', $title, array('title' => 'Save user data','class' => 'btn btn-primary submit-form-user','data-loading-text' => "Saving...")); ?>
    
    <?php echo anchor('users/', 'Cancel', array('title' => 'Go back to User List','class' => 'btn btn-danger')); ?>

    <?php echo form_close(); ?>

    <script type = 'text/javascript' src = "<?php echo base_url(); ?>js/users.js"></script> 
