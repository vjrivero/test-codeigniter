
<table class="table table-striped">
	<tr>
		<td>Name:</td>
		<td><?php echo $user->name;?></td>
	</tr>		
	<tr>
		<td>Email:</td>
		<td><?php echo $user->email;?></td>
	</tr>		
	<tr>
		<td>Phone Number:</td>
		<td><?php echo $user->phone;?></td>
	</tr>		
	<tr>
		<td>Age:</td>
		<td><?php echo $user->age;?></td>
	</tr>		
	<tr>
		<td>Role:</td>
		<td><?php if (!empty($userroles)) {
					$comma = "";
					foreach ($userroles as $value) {
						echo $comma; 
						echo $roles[$value['role_id']];
						$comma =", ";
					}
				}else{
					echo "No role assigned.";
				}
				?></td>
	</tr>		
</table>
<div class="pull-right">
	<?php echo anchor('users/edit/'.$user->id, '<span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Edit', array('title' => 'Edit user details','class' => 'btn btn-primary')); ?><br/>
</div>
<div class="pull-left">
	<?php echo anchor('users/', '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Back', array('title' => 'Go back to User List','class' => 'btn btn-primary')); ?>
</div>

