<html>
    <head>
            <title>CodeIgniter Test Project</title>
            <?php echo link_tag('css/style.css'); ?>
            <?php echo link_tag('css/bootstrap.css'); ?>

            <script type = 'text/javascript' src = "<?php echo base_url(); ?>js/jquery-3.1.1.min.js"></script>
            <script type = 'text/javascript' src = "<?php echo base_url(); ?>js/bootstrap.js"></script>
            <script type = 'text/javascript' src = "<?php echo base_url(); ?>js/commons.js"></script>
            <script type = 'text/javascript' >BASE_URL = "<?php echo base_url(); ?>";</script> 

    </head>
    <body>
    	<div class="">
    		<div class="col-md-10 col-md-offset-1">
		        <div class="page-header">
					<h1>User Management</h1>
				</div>
    		      
                <div class="page-header">
                    <h1><small><?php echo $title; ?></small></h1>
                </div>