<?php
class Users extends CI_Controller {

		/**
		 * Consturct function
		 */
        public function __construct()
        {
                parent::__construct();
                $this->load->model('users_model');
                $this->load->model('roles_model');
		        $this->load->helper('form');
                $this->load->helper('url_helper');
                $this->load->library('session');
        }


        /**
         * Index function: used to load all user into view.
         * @param 	integer $page (optional) offset to paginate.
         * @param 	String $filter (optional) filter param.
         * @return 	Array 	a list with all user into database. 
         */
        public function index($page = 0)
		{
		        $this->load->library('pagination');
		        $limit = 10;
		        $result = $this->users_model->get_users($page, $limit);
		        $data['users'] = $result['list'];
		        $data['title'] = 'User List';

		        if ($this->input->get('filter')) {
					$data['param'] = $this->input->get('filter');
		        }

		        //Config pagination
				$config = $this->_config_pagination('users/page/',$result['qty'],$limit);

				$this->pagination->initialize($config);
				
		        $this->_load_view('index', $data);
		}


        /**
         * View function: used to load user data into view.
         * @param 	integer $id user id.
         * @return 	Array 	an array with all user data. 
         */
		public function view($id = FALSE)
		{
		        $data['user'] = $this->users_model->get_user($id);
		        $data['userroles'] = $this->users_model->get_roles($id);
		        $data['roles'] = $this->roles_model->get_roles_to_select();
	        	$data['title'] = 'User Details';

		        $this->_load_view('view', $data);
		}


        /**
         * Delete function: used to delete user data from database.
         * @param 	integer $id user id.
         */
		public function delete($id = FALSE)
		{
				if ($id) {
			        $data['users'] = $this->users_model->delete_user($id);
			        $this->session->set_flashdata('success_message', "User deleted successfully!");
				}
		        redirect('users');
		}


        /**
         * Add function: used to add/update user data to database.
         * @param 	integer $id (optional) user's id to update. 
         */
		public function add($id = FALSE)
		{
			    $this->load->library('form_validation');

			    $this->form_validation->set_rules('name', 'Name', 'required');
			    $this->form_validation->set_rules('email', 'Email', 'required');
			    $this->form_validation->set_rules('role[]', 'Role', 'required');

		        
			    if ($this->form_validation->run() === FALSE)
			    {
		        	$data['roles'] = $this->roles_model->get_roles_to_select();
		        	$selected = array();

			    	if (!$id) 
			    	{
				        $data['title'] = 'Add User';
				        $data['selected'] = $selected;
			        	$this->_load_view('add', $data);
			    	}
			        else
			        {
			        	$data['user'] = $this->users_model->get_user($id);
			        	$userroles = $this->users_model->get_roles($id);
			        	if (!empty($userroles)) {
			        		foreach ($userroles as $value) {
			        			array_push($selected, $value['role_id']);
			        		}
			        	}
			        	$data['title'] = 'Edit User';
				        $data['selected'] = $selected;
			        	$this->_load_view('add', $data);
			        }
			    }
			    else
			    {
			        $this->users_model->set_user();
			        $action = $this->input->post('id') ? "updated" : "created";
			        $this->session->set_flashdata('success_message', "User $action successfully!");
			        redirect('users');
			    }

		}


        /**
         * Check Email function: used to validate if an email already exist into database.
         * @param 	String 	$email user email.
         * @param 	integer $id (optional) user id.
         * @return 	String 	"SUCCESS" if the email exist and "ERROR" if not. 
         */
		public function check_email()
		{
		        $user = $this->users_model->check_email();
		        if ($user) {
		        	echo "SUCCESS";
		        }else {
		        	echo "ERROR";
		        }
		}


        /**
         * Config Pagination function: (Private) used to set config pagination.
         * @param 	String 	$URL url to function page.
         * @param 	integer $qty total rows count from query.
         * @param 	integer $limit record per page.
         * @return 	Array 	with all parameters to config pagination. 
         */
		private function _config_pagination($URL, $qty, $limit)
		{
			$config['base_url'] = base_url().$URL;
			$config['total_rows'] = $qty;
			$config['per_page'] = $limit;
			$config['num_links'] = 4;
			$config['reuse_query_string'] = TRUE;

			//Html structure pagination 
			$config['full_tag_open'] = '<ul class="pagination pagination-sm">';
			$config['full_tag_close'] = '</ul>';
			$config['first_link'] = FALSE;
			$config['last_link'] = FALSE;
			$config['prev_link'] = '<span aria-hidden="true">&laquo;</span>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = '<span aria-hidden="true">&raquo;</span>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';

			return $config;
		}


        /**
         * Load View function: (Private) used to load specific view with custom templates.
         * @param 	String 	$page name page to load.
         * @param 	Array 	$data with all parameters to load into view.
         */
        private function _load_view($page, $data)
        {
        	if ( ! file_exists(APPPATH.'views/users/'.$page.'.php'))
	        {
	                // Whoops, we don't have a page for that!
	                show_404();
	        }

        	$this->load->view('templates/header', $data);
	        $this->load->view('users/'.$page, $data);
	        $this->load->view('templates/footer', $data);
        }
}