var URL_DELETE_USER = "";

$(document).ready(function(){
	$('.submit-form-user').click(function(evt){
		evt.preventDefault();
		var $btn = $(this).button('loading');
	    if (form_validate()) {
	    	var id = $('input[name=id]').length ? $('input[name=id]').val() : '';
			check_user_email($('#email').val(), id);
		}else{
	    	$btn.button('reset');
		}
	});

	$('#form-search').on('submit', function(evt){
		evt.preventDefault();
		var FILTER = $('#searchInput').val() == '' ? '' : "?filter="+$('#searchInput').val();
		window.location = $('#form-search').attr('action') + FILTER;
	});

	$('.delete-user').click(function(){
		var $MODAL = $('#myModal');
		$MODAL.find('.user-name').text($(this).attr('data-user'));
		$MODAL.modal('show');
		URL_DELETE_USER = $(this).attr('href');
	});

});

function form_validate(){
	var valid = true;

	if ($('#name').val() == "" ) {
		valid = false;
		$('.input-name').addClass('has-error');
		$('.input-name').find('.form-control-feedback').removeClass('hidden');
		$('.input-name').find('.help-block').removeClass('hidden');
	}else{
		$('.input-name').removeClass('has-error');
		$('.input-name').find('.form-control-feedback').addClass('hidden');
		$('.input-name').find('.help-block').addClass('hidden');
	}

	if ($('#email').val() == "" || !isEmail($('#email').val())) {
		valid = false;
		$('.input-email').addClass('has-error');
		$('.input-email').find('.form-control-feedback').removeClass('hidden');
		$('.input-email').find('.help-block').addClass('hidden');
		$('.input-email').find('.help-block.email-invalid').removeClass('hidden');
	}else{
		$('.input-email').removeClass('has-error');
		$('.input-email').find('.form-control-feedback').addClass('hidden');
		$('.input-email').find('.help-block').addClass('hidden');
	}

	if ($('select').val() == "" ) {
		valid = false;
		$('.input-roles').addClass('has-error');
		$('.input-roles').find('.help-block').removeClass('hidden');
	}else{
		$('.input-roles').removeClass('has-error');
		$('.input-roles').find('.help-block').addClass('hidden');
	}

	return valid;
}

function check_user_email(email,id){
	$.ajax({
		url: BASE_URL+"users/check_email", 
		type: "GET", 
		data: {email:email, id:id}, 
		success: function(result){
			if (result == "SUCCESS") {
				$('.input-email').addClass('has-error');
				$('.input-email').find('.form-control-feedback').removeClass('hidden');
				$('.input-email').find('.help-block').addClass('hidden');
				$('.input-email').find('.help-block.email-exist').removeClass('hidden');
				$('.submit-form-user').button('reset');
			} else
			{
				console.log("submit");
				$('body').find('#user-form').submit();
			} 
		}
	});
}

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function deleteUser(){
	$('.delete-button').button('loading');
	window.location = URL_DELETE_USER; 
}

