# TEST CODEIGNITER - PROJECT USER MANAGEMENT

Mini proyecto gestionador de usuarios dentro de una base de datos. 

## Herramientas y tecnologías usadas

- [Codeigniter 3.1.3](https://www.codeigniter.com/)
- Manejador de Base de Datos MySQL 5.6.24
- PHP 5.6.8
- [Bootstrap 3.3.7](http://getbootstrap.com/getting-started/)
- [jQUery 3.1.1](http://jquery.com/download/)

## Instalación 

1. Descargar del repositorio `git clone https://vjrivero@bitbucket.org/vjrivero/test-codeigniter.git`
2. Mover el contenido a la carpeta `www` o `public_html` según sea el caso.
3. Ejecutar el script SQL ubicado dentro de la carpeta `[folder_root]/application/config/schema/`.
  * **NOTA:** _Por defecto, se incluyen 4 roles en el script_.
4. Condigurar la conexión a la base de datos en `\application\config\database.php`.

## Funcionalidades desarrolladas

- CRUD de usuarios.
- Validaciones del lado frontend y backend.
- Validaciones asíncronas.
- Filtro de usuarios en el listado y paginación.

###### CRUD de usuarios

Permite crear, leer, modificar y eliminar usuarios de la base de datos. 

###### Validaciones 

- Se verifica que los usuarios tengan nombre y correo, así como al menos un rol asignado.
- También se verifica que el correo sea único para cada usuario, usando consulta asíncrona a la Base de Datos. Adicionalmente, se especificó en la Tabla usuarios que dicho campo sea único.
- El campo número de teléfono y edad son númericos. Validaciones hechas con `input type number` (HTML5). De la misma manera se verfica que el correo suministrado tenga un formato válido, adicional a validación con `JavasScript`.

###### Filtro y paginación en listado de usuarios

- Permite buscar un usario comparando el parámetro indicado con los nombres de usuario o sus correos.
- Los resultados están paginados en 10 registros por cada página mostrada.
- Si se muestra una lista con filtro, la paginación conserva el criterio de búsqueda.

## Notas adicionales

A pesar que usar campos HTML5 como `type number` para restringir al usuario que solo introduzca valores numéricos, validaciones para campos como edad, que no debería tener una longitud de caracteres superior a 2 (máx 3), en ciertos navegadores es omitido el atributo `maxlength` para ese tipo de campo. Por esto, debe aplicarse otros métodos de validaciones en casos en que sea necesario tales restricciones.
